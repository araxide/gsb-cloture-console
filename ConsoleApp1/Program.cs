﻿using System;
using System.Globalization;
using System.Threading;

namespace GSB_CLOTURE
{
    /// <summary>
    /// Point d'entrée principal de l'application console
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// Classe exécutable
        /// </summary>
        public static void Main()
        {
            Timer t = new Timer(TimerCallback, null, 0, 2000);
            // Wait for the user to hit <Enter>
            Console.ReadLine();

        }

        /// <summary>
        /// Code qui s'exécute à chaque intervalle
        /// </summary>
        /// <param name="o">objet correspondant au callback du timer</param>
        private static void TimerCallback(Object o)
        {
            // on établit un accès à la base
            AccesDonnees acces = new AccesDonnees();

            // quel est le mois précédent ?
            DateTime maDate = DateTime.ParseExact("21/02/2019", "dd/MM/yyyy", CultureInfo.InvariantCulture);// si besoin de mettre une date différente de la date du jour
            String moisPrecedent = GereDates.getMoisPrecedent(maDate);
            // de quelle année ? 
            String annee = maDate.ToString("yyyy");
            // on assemble les deux pour pouvoir identifier les fiches dans la base 
            String moisId = annee+moisPrecedent;
            // Console.WriteLine(moisId);
            //test sur le jour "entre"
            int jourComparaison = maDate.Day;
            // Console.WriteLine(jourComparaison);
            // on va vérifier qu'on est entre le 1 et le 10
            if (GereDates.entre(1, 10, maDate))
            {
                // on indique les fiches qui vont être modifiées
                String sb = acces.requeteLecture("select * from fichefrais where mois =" + moisId + " and idetat='CR'");
                Console.WriteLine(sb);
                // on met à jour les fiches
                acces.requeteAdmin("update fichefrais set idetat='CL' where mois =" + moisId + " and idetat='CR'");
            }
            else if (GereDates.entre(20, 31, maDate))
            {
                String sb = acces.requeteLecture("select * from fichefrais where mois =" + moisId + " and idetat='MP'");
                Console.WriteLine(sb);
                acces.requeteAdmin("update fichefrais set idetat='RB' where mois =" + moisId + " and idetat='MP'");
            }

            // on ferme l'accès à la base 
            acces.close();

        }

    }
}

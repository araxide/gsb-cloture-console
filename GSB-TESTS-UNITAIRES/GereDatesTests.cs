﻿using System;
using GSB_CLOTURE;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;

namespace GSB_CLOTURE.tests
{
    /// <summary>
    /// Classe de tests pour les dates
    /// </summary>
    [TestClass]
    public class GereDatesTests
    {
        /// <summary>
        /// Teste le mois précédent à la date du jour
        /// </summary>
        [TestMethod]
        public void getMoisPrecedentTest()
        {
            string moisPrecedent = GereDates.getMoisPrecedent();
            Assert.AreEqual("02", moisPrecedent);
        }

        /// <summary>
        /// Teste le mois précédent à une date différente de la date du jour
        /// </summary>
        [TestMethod]
        public void getMoisPrecedentSurchargeTest()
        {
            DateTime maDate = DateTime.ParseExact("20/07/2019", "dd/MM/yyyy", CultureInfo.InvariantCulture);
            string moisPrecedent = GereDates.getMoisPrecedent(maDate);
            Assert.AreEqual("06", moisPrecedent);
        }

        /// <summary>
        /// Teste le mois suivant à la date du jour
        /// </summary>
        [TestMethod]
        public void getMoisSuivantTest()
        {
            string moisSuivant = GereDates.getMoisSuivant();
            Assert.AreEqual("04", moisSuivant);
        }

        /// <summary>
        /// Teste le mois suivant à une date différente de la date du jour
        /// </summary>
        [TestMethod]
        public void getMoisSuivantSurchargeTest()
        {
            DateTime maDate = DateTime.ParseExact("20/07/2019", "dd/MM/yyyy", CultureInfo.InvariantCulture);
            string moisSuivant = GereDates.getMoisSuivant(maDate);
            Assert.AreEqual("08", moisSuivant);
        }

        /// <summary>
        /// Teste si la date du jour se situe entre deux bornes
        /// </summary>
        [TestMethod]
        public void entreTest()
        {
            int jour1 = 20;
            int jour2 = 31;
            Boolean test = GereDates.entre(jour1, jour2);
            Assert.AreEqual(false, test);
        }

        /// <summary>
        /// Teste si une date différente de la date du jour se situe entre deux bornes
        /// </summary>
        [TestMethod]
        public void entreSurchargeTest()
        {
            int jour1 = 20;
            int jour2 = 31;
            DateTime maDate = DateTime.ParseExact("20/07/2019", "dd/MM/yyyy", CultureInfo.InvariantCulture);
            Boolean test = GereDates.entre(jour1, jour2, maDate);
            Assert.AreEqual(true, test);
        }

    }
}

# Application Console Clôture des frais

** _Note sur l'avancement du projet_ **

L'application est terminée.

## ** Technologies utilisées **

Base de données SQL

Visual Studio Enterprise 2017

C# utilisant le framework .NET Core 2.0 

## ** But de l'application **

Ce projet a été codé dans le cadre du contexte GSB pour le BTS SIO (session 2019). 

Il s'agit de pouvoir se connecter à une base de données MySQL pour effectuer des vérifications et des modifications selon la date du jour. La base de données recense des fiches de frais mensuelles et doit modifier périodiquement leur état. Ainsi : 

* à partir du 1er jour du mois N, et jusqu'au 10e jour, toutes les fiches non clôturées (CR) crées au mois N-1 passent en statut CL (clôturé) *
 
* à partir du 20e jour du mois N, les fiches crées au mois N-1 et mises en paiement (MP) passent en statut RB (remboursement) * 

Le projet comporte un exécutable sous forme d'application console, une bibliothèque de classes avec une classe d'accès aux données et une classe pour la gestion des dates, et un projet de tests unitaires.

## ** Note importante sur l'utilisation **

La base de données distante utilisée dans le cadre du projet ne sera disponible que jusque fin juin 2019. 
Après cette date, vous devrez implémenter la base sur votre propre serveur à l'aide du script SQL nommé gsb_frais, et modifier les paramètres de connexion dans la méthode InitConnexion() de la classe AccesDonnees.cs


